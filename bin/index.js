#!/usr/bin/env node

const bossy = require('bossy');

const path = require('path');
const { requireFiles } = require('hapi-utility-belt');

const flags = require('./flags');
const commands = require('./commands');
const konsole = require('../utils/konsole');
const { isCommand, whichCommand } = require('../utils/commands');
const { isTmpl } = require('../utils/templates');

const args = bossy.parse(flags);

const cmd = args._ && args._.shift();

if (!cmd) {

    konsole.error('You must enter a command');
}

if (!isCommand(cmd)) {

    konsole.error('Command does not exist. Run "help" for list of commands.');
}

cmd = whichCommand(cmd);

commands[cmd](args);

