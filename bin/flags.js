module.exports = {
    m: {
        description: 'Method',
        alias: 'method'
    },
    p: {
        description: 'Path',
        alias: 'path'
    },
    n: {
        description: 'Name',
        alias: 'name'
    },
    u: {
        description: 'URL',
        alias: 'url'
    }
};
