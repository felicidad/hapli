const commands = {

    f: 'file',
    s: 'snippet',
    S: 'scaffold'
};

const subCommands = [
    'plugin',
    'route',
    'config',
    'module',
    'module-array',
    'test',
    'server',
    'labrc'
];

const scaffolds = [
    'crud',
    'flat-module',
    'plugins-config'
];

const isCommand = (name) => {
    return Object.keys(commands).indexOf(name) > -1 ||
           Object.values(commands).indexOf(name) > -1;
}

const isSubCommand = (name) => subCommands.indexOf(name) > -1;
const isScaffold = (name) => scaffolds.indexOf(name) > -1;

const whichCommand = (name) => {

    const keys = Object.keys(commands);
    const values = Object.values(commands);

    const k = keys.indexOf(name);
    const v = values.indexOf(name);

    if (k > -1) {

        return values[k];
    }

    return values[v];
}

module.exports = {
    commands,
    isCommand,
    whichCommand,
    subCommands,
    scaffolds,
    isSubCommand,
    isScaffold
};
