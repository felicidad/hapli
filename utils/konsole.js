const chalk = require('chalk');

const prefix = {

    log: '\u2023',
    warn: '\u26A0',
    error: '\u2718',
    success: '\u2714',
};

const color = {
    log: 'cyan',
    warn: 'yellow',
    error: 'red',
    success: 'green'
};

const logger = function (which, args) {

    const _args = Array.from(args);
    _args.unshift(prefix[which]);

    console.log(chalk[color[which]].apply(chalk, _args));
}

module.exports = {

    log: function () {

        logger('log', arguments);
    },
    warn: function () {

        logger('warn', arguments);
    },
    error: function () {

        logger('error', arguments);
        process.exit(1);
    },
    success: function () {

        logger('success', arguments);
    },

}
