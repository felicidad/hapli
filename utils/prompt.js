const rl = require('readline');

module.exports = (question) =>  {

    return new Promise((resolve, reject) => {

        const r = rl.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        r.question(question + '\n', function(answer) {

            r.close();
            resolve(answer);
        });
    });
}
