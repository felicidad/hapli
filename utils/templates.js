const fs = require('fs');
const konsole = require('./konsole');

const ejs = require('ejs');

const tmplPath = (name) => path.resolve(__dirname, '..', 'templates', name);

const getTmpl = (name) => {

    if (isTmpl(name)) {

        return fs.readFileSync(tmplPath(name));
    }

    konsole.error(chalk.red('Template does not exist'));
}

const tmplCache = {};

const generateTemplate = async (name, data) => {

    if (!tmplCache[name]) {

        tmplCache[name] = getTmpl(name);
    }

    return await ejs.render(tmplCache[name], data, { async: true });
}

module.exports = {
    getTmpl,
    generateTemplate
};
