const prompt = require('./prompt');

module.exports = async (question) => {

    const answer = await prompt(question + ' (y|yes)');
    return /(yes|y)/i.test(answer);
}
